//
//  AppDelegate.swift
//  SMCustomerCare
//
//  Created by oneweekstudio on 05/20/2020.
//  Copyright (c) 2020 oneweekstudio. All rights reserved.
//

import UIKit
import SMCustomerCare

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

