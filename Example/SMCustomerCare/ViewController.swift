//
//  ViewController.swift
//  SMCustomerCare
//
//  Created by oneweekstudio on 05/20/2020.
//  Copyright (c) 2020 oneweekstudio. All rights reserved.
//

import UIKit
import SMCustomerCare

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapFeedbackButton(_ sender: Any) {
        showFeedBackWithMoreInfo()
    }
    
    
    //Pháp 1
    func showFeedBackWithDefault() {
        //Default ( light Mode )
        let vc = SMCustomerCare()
        vc.present(from: self , delegate: self,projectId: "du02")

    }
    
    //Pháp 2
    func showFeedBackWithDarkMode() {
        
        let feed2  = SMCustomerCare.init()
        
        let color = SMCustomCareDefaultColor.init(navColor:         UIColor.appColor(CTAssetColor.navigationBackground),
                                                  tintColor:        UIColor.appColor(CTAssetColor.title),
                                                  bgColor:          UIColor.appColor(CTAssetColor.background),
                                                  txtColor:         UIColor.appColor(CTAssetColor.title),
                                                  placeHolderColor: UIColor.appColor(CTAssetColor.placeHolder))
        DispatchQueue.main.async {
            feed2.present(from: self , delegate: self , customColor: color,projectId: "du02" , animated: true , completion: nil)
        }
    }
    
    //Pháp 3
    func showFeedBackWithDarkMode2() {
        
        let newFeed = SMCustomerCare()
            .setBackground(withColorSet: "background", defaultColor: UIColor.red)
            .setNavigationBackground(withColorSet: "navigationBackground", defaultColor: UIColor.blue)
            .setNavigationTint(withColorSet: "navigationTint", defaultColor: .yellow)
            .setText(withColorSet: "title", defaultColor: .cyan)
            .setPlaceHolder(withColorSet: "placeHolder", defaultColor: .lightGray)
        
        DispatchQueue.main.async {
            newFeed.present(from: self , delegate: self,projectId: "du02")
        }
    }
    
    //Pháp 4
    func showFeedBackWithDarkMode4() {
        
        let newFeed = SMCustomerCare()
        
        newFeed.setBackgroundColor(withColorSet: "background", defaultColor: UIColor.red)
        newFeed.setNavigationBackgroundColor(withColorSet: "navigationBackground", defaultColor: UIColor.blue)
        newFeed.setNavigationTintColor(withColorSet: "navigationTint", defaultColor: .yellow)
        newFeed.setTextColor(withColorSet: "title", defaultColor: .cyan)
        newFeed.setPlaceHolderColor(withColorSet: "placeHolder", defaultColor: .lightGray)
        
        DispatchQueue.main.async {
            newFeed.present(from: self , delegate: self,projectId: "du02")
        }
    }
    
    //More Info
    func showFeedBackWithMoreInfo() {
        
        let newFeed = SMCustomerCare()
        newFeed.present(from: self , delegate: self ,projectId: "du02", moreInfo: ["More Info Key":"More Info Value","Key 2":"Value 2"])
    }
    
}

enum CTAssetColor: String, SMColorProtocol {
    
    case navigationBackground
    case navigationTint
    case background
    case title
    case placeHolder
    
    public var getAssetColorName: String {
        return self.rawValue
    }
    
    public var defaultColor: UIColor {
        switch self {
        case .navigationBackground:
            return UIColor(red: 0.969, green: 0.969, blue: 0.969, alpha: 1.0)
        case .navigationTint:
            return UIColor.black
        case .background:
            return UIColor.white
        case .title:
            return UIColor.black
        case .placeHolder:
            return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        }
    }
}


extension ViewController : SMCustomerCareDelegate {

    //Khi người dùng ấn nút show feedback
    func smCustomerCareDidPresentFeedback() {
        print("-------> smCustomerCareDidPresentFeedback")
    }
    
    //Khi người dùng close màn feedback
    func smCustomerCareDidClose() {
        print("-------> smCustomerCareDidClose")
    }
    
    //Khi người dùng gửi feedback
    func smCustomerCareDidSend() {
        print("-------> smCustomerCareDidSend")
    }
    
    //Khi người dùng thêm ảnh
    func smCustomerCareDidAddImage() {
        print("-------> smCustomerCareDidAddImage")
    }
    
    //Khi người dùng gửi thảnh công lên server
    func smCustomerCareDidSendFeedbackSuccess() {
        print("-------> smCustomerCareDidSendFeedbackSuccess")
    }
    
    //Khi người dùng gửi feed back lỗi trên server
    func smCustomerCareDidSendFeedbackError(_ optionalError : Error?) {
        print("-------> smCustomerCareDidSendFeedbackError : \(optionalError?.localizedDescription ?? "nil")")
    }
    
}
