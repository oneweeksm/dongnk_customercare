# Acknowledgements
This application makes use of the following third party libraries:

## JGProgressHUD

The MIT License (MIT)

Copyright (c) 2014-2018 Jonas Gessner

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## SMCustomerCare

Copyright (c) 2020 oneweekstudio <dongnd@smartmove.com.vn>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


## UITextView+Placeholder

The MIT License (MIT)

Copyright (c) 2014 Suyeol Jeon (http://xoul.kr)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


## ZendeskCommonUISDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskCoreSDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskMessagingAPISDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskMessagingSDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskSDKConfigurationsSDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskSupportProvidersSDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.


## ZendeskSupportSDK

ZendeskSDKs
Created by Zendesk on  1/09/2020
Copyright (c) 2020 Zendesk. All rights reserved.
By downloading or using the Zendesk Mobile SDK, You agree to the Zendesk Master
Subscription Agreement https://www.zendesk.com/company/customers-partners/#master-subscription-agreement and Application Developer and API License
Agreement https://www.zendesk.com/company/customers-partners/#application-developer-api-license-agreement and
acknowledge that such terms govern Your use of and access to the Mobile SDK.

Generated by CocoaPods - https://cocoapods.org
