#
# Be sure to run `pod lib lint SMCustomerCare.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SMCustomerCare'
  s.version          = '2.0.1'
  s.summary          = 'Sản phẩm thuộc về ONEWEEK STUDIO'
  s.swift_versions   = '5.0'
  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  
  s.description      = <<-DESC
  TODO: Client gửi lên gốm các params: email (required), comment(required), Tên app (required), bundle id (required), version (required), build number (required) , image (required)
  DESC
  
  s.homepage         = 'https://bitbucket.org/oneweeksm/dongnk_customercare/src/master'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'oneweekstudio' => 'smartmove.vn' }
  s.source           = { :git => 'https://bitbucket.org/oneweeksm/dongnk_customercare/src/master', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '11.0'
  
  s.source_files = 'SMCustomerCare/Classes/*'
  s.resource_bundles = {
      'SMCustomerCare' => ['SMCustomerCare/Assets/*']
  }
  
  s.static_framework = true
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.ios.dependency 'UITextView+Placeholder'
  s.ios.dependency 'ZendeskSupportSDK'
  s.ios.dependency 'JGProgressHUD'
end
