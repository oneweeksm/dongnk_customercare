//
//  SMFeedbackViewController.swift
//  Pods
//
//  Created by OW01 on 5/20/20.
//

import Foundation
import Photos
import UITextView_Placeholder
import JGProgressHUD

class SMFeedbackViewController : UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var vContainerContent: UIView!
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var bottomOfCollectionView : NSLayoutConstraint!
    
    var delegate : SMCustomerCareDelegate? = nil 
    var model : SMFeedbackModel = SMFeedbackModel()
    var moreInfo : [String:Any]? = nil
    var projectId : String = ""
    
    var btnSend : UIButton!
    var btnClose: UIButton!
    let hud = JGProgressHUD()
    
    var iColor: SMCustomCareDefaultColor!
    
    //MARK: UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.model.projectId = self.projectId
        self.model.moreInfo = self.moreInfo
        setupNavigation()
        
        setupCollectionView()
        
        addDoneButtonOnKeyboard()
        
        setupEmailTextField()
        
        setUIColor()
        
    }
    
    deinit {
        print("Feedback deinit")
    }
    
}

// MARK: - Private function
extension SMFeedbackViewController {
    
    func setUIColor() {
        navigationController?.navigationBar.barTintColor = iColor.navigationBar
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: iColor.text]
        
        self.view.backgroundColor = iColor.background
        self.textFieldEmail.textColor = iColor.text
        self.textFieldEmail.attributedPlaceholder = NSAttributedString(string: "Your email address (required)",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: iColor.placeHolder])
        self.txtView.textColor = iColor.text
        self.txtView.placeholderColor = iColor.placeHolder
        self.txtView.delegate = self
    }
    
    func setupEmailTextField() {
        
        self.textFieldEmail.delegate = self
        textFieldEmail.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        textFieldEmail.setLeftPaddingPoints(5)
        textFieldEmail.setRightPaddingPoints(5)
        self.showBottom(isShow: true)
    }
    
    func setupNavigation() {
        title = "Feedback"
        
        addCloseButton()
        
        addSendButton()
    }
    
    func showBottom(isShow:Bool){
        UIView.animate(withDuration: 0.3) {
            self.viewBottom.alpha = isShow ? 1 : 0
            self.bottomOfCollectionView.constant = isShow ? 8 : -180
        }
    }
    
    func addCloseButton() {
        btnClose = UIButton.init(type: .custom)
        let image = loadImageFromResources(imageName: "icon_sm_feedback_close")?.imageWithColor(smColor: iColor.text)
        btnClose.setImage(image, for: .normal)
        btnClose.imageEdgeInsets = UIEdgeInsets.init(top: 8, left: 0, bottom: 8, right: 16)
        btnClose.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        btnClose.addTarget(self, action: #selector(self.actionTapClose), for: .touchUpInside)
        let item = UIBarButtonItem.init(customView: btnClose)
        navigationItem.leftBarButtonItem = item
    }
    
    @objc private func actionTapClose() {
        print("Close Feedback")
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            
            self.dismiss(animated: true) {
                self.delegate?.smCustomerCareDidClose?()
            }
        }
        
    }
    
    func addSendButton() {
        btnSend = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        btnSend.setTitle("Send", for: .normal)
        btnSend.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSend.setTitleColor(iColor.tint, for: .normal)
        btnSend.setTitleColor(UIColor.gray, for: .disabled)
        btnSend.addTarget(self , action: #selector(self.actionTapSend), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnSend)
        btnSend.isEnabled = false
    }
    
    @objc private func actionTapSend() {
        print("Send Feedback")
        self.delegate?.smCustomerCareDidSend?()
        self.model.content = self.txtView.text.trim()
        if !self.model.isValid() {
            self.showErrorEmptyContent()
        } else {
            showHud()
            SMCustomerServices.createIdentify(email: self.model.email)
            
            self.model.uploadMultiAttackments { isSuccess in
                if isSuccess {
                    self.postFeedback()
                }else{
                    print("upload failed")
                    self.showErrorSentFailed()
                    self.dismissHud()
                }
            }
        }
    }
    func postFeedback(){
        SMCustomerServices.postToZendesk(model: self.model) { isSuccess , error in
            if isSuccess {
                print("post success")
                self.dismiss(animated: true) {
                    self.delegate?.smCustomerCareDidSendFeedbackSuccess?()
                }
            }else{
                print("post failed")
                self.delegate?.smCustomerCareDidSendFeedbackError?(error)
                self.showErrorSentFailed()
            }
            self.dismissHud()
        }
    }
    
    func deleteAttackment(index:Int){
        self.showAlertDeleteImage { [weak self] in
            guard let `self` = self else { return }
            self.model.removeImage(index: index)
            self.collectionView.reloadData()
        }
    }
    
    func showHud(){
        if let window = UIApplication.shared.delegate?.window as? UIWindow {
            hud.textLabel.text = "Sending..."
            hud.show(in: window)
        }
    }
    func dismissHud(){
        hud.dismiss(afterDelay: 1.0)
    }
    func setupCollectionView() {
        let bundle = getSMCustomCareBundlePath()
        

        let nib = UINib.init(nibName: "SMImageCollectionViewCell", bundle: bundle)
        collectionView.register(nib, forCellWithReuseIdentifier: "SMImageCollectionViewCell")
        
        let nib2 = UINib.init(nibName: "SMNewImageCollectionViewCell", bundle: bundle)
        collectionView.register(nib2 , forCellWithReuseIdentifier: "SMNewImageCollectionViewCell")
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtView.inputAccessoryView = doneToolbar
        textFieldEmail.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        txtView.resignFirstResponder()
        textFieldEmail.resignFirstResponder()
    }
    
}


extension SMFeedbackViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? 1 : self.model.attackments.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return indexPath.section == 0 ?
            setSMAddImageCollectionViewCell(collectionView, cellForItemAt: indexPath) :
            setSMImageCollectionViewCell(collectionView, cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.bounds.size.height * 0.65 , height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.openGallery()
            
        }
        
    }
    
    func setSMImageCollectionViewCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SMImageCollectionViewCell", for: indexPath) as! SMImageCollectionViewCell
        let item = self.model.attackments[indexPath.item]
        cell.config(attack: item)
        cell.onTapRemoveButton = { [weak self] in
            guard let `self` = self else { return }
            self.deleteAttackment(index: indexPath.item)
        }
        
        return cell
    }
    
    func setSMAddImageCollectionViewCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SMNewImageCollectionViewCell", for: indexPath) as! SMNewImageCollectionViewCell
        return cell
    }
    
    
}

// MARK: - Image

extension SMFeedbackViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func openGallery() {
        
        self.requestAuthorization()
        
    }
    
    func showErrorPermissionGallery() {
        DispatchQueue.main.async {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func requestAuthorization() {
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .authorized {
            self.load()
        } else {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    self.load()
                } else {
                    self.showErrorPermissionGallery()
                }
            })
        }
        
    }
    
    
    func load() {
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.mediaTypes = ["public.image"] //"public.movie"
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    //MARK:-- ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImageUrl = info[.imageURL] as? URL {
            DispatchQueue.main.async {
                self.delegate?.smCustomerCareDidAddImage?()
                self.model.addImage(url: pickedImageUrl)
                self.btnSend.isEnabled = self.model.isValid()
                self.collectionView.reloadData()
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - Alert
extension SMFeedbackViewController {
    
    func showAlertDeleteImage(completion: (() -> Void)?) {
        DispatchQueue.main.async {
            // create the alert
            let alert = UIAlertController(title: "Notice", message: "*This action will remove this image!*", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Remove", style: UIAlertAction.Style.destructive, handler: {action in
                completion?()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
}



extension SMFeedbackViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let mail = textField.text  else {
            return
        }
        self.model.email = mail.trim()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let mail = textField.text  else {
            self.btnSend.isEnabled = false
            return
        }
        self.model.email = mail.trim()
        self.btnSend.isEnabled = self.model.isValid()
    }
    
    func showErrorEmptyContent() {
        DispatchQueue.main.async {
            let alert  = UIAlertController(title: "Message", message: "The email, content or photos is not empty!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showErrorSentFailed() {
        DispatchQueue.main.async {
            let alert  = UIAlertController(title: "Sent failed!", message: "Please check your connnection and try again!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}


extension SMFeedbackViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text ,
           !text.trim().isEmpty {
            self.model.content = text.trim()
            self.btnSend.isEnabled = self.model.isValid()
        }
    }
}
