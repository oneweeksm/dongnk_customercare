//
//  SMCustomerCare.swift
//  SMCustomerCare
//
//  Created by OW01 on 5/20/20.
//

import Foundation
import ZendeskCoreSDK
import SupportSDK

@objc public protocol SMCustomerCareDelegate {
    
    //Khi người dùng ấn nút show feedback
    @objc optional func smCustomerCareDidPresentFeedback()
    
    //Khi người dùng close màn feedback
    @objc optional func smCustomerCareDidClose()
    
    //Khi người dùng gửi feedback
    @objc optional func smCustomerCareDidSend()
    
    //Khi người dùng thêm ảnh
    @objc optional func smCustomerCareDidAddImage()
    
    //Khi người dùng gửi thảnh công lên server
    @objc optional func smCustomerCareDidSendFeedbackSuccess()
    
    //Khi người dùng gửi feed back lỗi trên server
    @objc optional func smCustomerCareDidSendFeedbackError(_ optionalError : Error?)
    
}

public protocol SMCustomerCareLogic {
        
    func present(from sender: UIViewController,
                        delegate:SMCustomerCareDelegate,
                        customColor: SMCustomCareDefaultColor,
                        projectId:String,
                        moreInfo:[String:Any]?,
                        animated: Bool,
                        completion:(()->Void)? )
    
    func present(from sender: UIViewController,
                        delegate:SMCustomerCareDelegate,
                        projectId:String,
                        moreInfo:[String:Any]?,
                        animated: Bool,
                        completion: (() -> Void)?)
    
    func setColor( color: SMCustomCareDefaultColor)
    
}


open class SMCustomerCare : NSObject, SMCustomerCareLogic {
    
    //MARK:- SMCustomerCareDelegate
    private var color: SMCustomCareDefaultColor = SMCustomCareDefaultColor()
    
    public func setColor( color: SMCustomCareDefaultColor) {
        self.color = color
    }
    
    //MARK: - Cài đặt Zendesk (required)
    
    private func registerZendesk(){
        Zendesk.initialize(appId: ZendeskInfo.appId, clientId: ZendeskInfo.clientId, zendeskUrl: ZendeskInfo.url)
        Support.initialize(withZendesk: Zendesk.instance)
    }
    
    
    
    //MARK: - Cài đặt color tùy chỉnh trả về chính class này ( Wraper )
    public func setNavigationBackground( withColorSet name : String, defaultColor : UIColor) -> Self {
        //color.navigationBar = self.getColor(colorSet: name, defaultColor: defaultColor)
        self.setNavigationBackgroundColor(withColorSet: name, defaultColor: defaultColor)
        return self
    }
    
    public func setNavigationTint( withColorSet name : String, defaultColor : UIColor) -> Self {
//        color.tint = self.getColor(colorSet: name, defaultColor: defaultColor)
        self.setNavigationTintColor(withColorSet: name, defaultColor: defaultColor)
        return self
    }
    
    public func setBackground( withColorSet name : String, defaultColor : UIColor) -> Self {
//        color.background = self.getColor(colorSet: name, defaultColor: defaultColor)
        self.setBackgroundColor(withColorSet: name, defaultColor: defaultColor)

        return self
    }
    
    public func setText( withColorSet name : String, defaultColor : UIColor) -> Self {
//        color.text = self.getColor(colorSet: name, defaultColor: defaultColor)
        self.setTextColor(withColorSet: name, defaultColor: defaultColor)

        return self
    }
    
    public func setPlaceHolder( withColorSet name : String, defaultColor : UIColor) -> Self {
//        color.placeHolder = self.getColor(colorSet: name, defaultColor: defaultColor)
        self.setPlaceHolderColor(withColorSet: name, defaultColor: defaultColor)

        return self
    }
    
    //MARK: - Cài đặt color tùy chỉnh
    public func setNavigationBackgroundColor( withColorSet name : String, defaultColor : UIColor) -> Void {
        color.navigationBar = self.getColor(colorSet: name, defaultColor: defaultColor)
        return
    }
    
    public func setNavigationTintColor( withColorSet name : String, defaultColor : UIColor) -> Void {
        color.tint = self.getColor(colorSet: name, defaultColor: defaultColor)
        return
    }
    
    public func setBackgroundColor( withColorSet name : String, defaultColor : UIColor) -> Void {
        color.background = self.getColor(colorSet: name, defaultColor: defaultColor)
        return
    }
    
    public func setTextColor( withColorSet name : String, defaultColor : UIColor) -> Void {
        color.text = self.getColor(colorSet: name, defaultColor: defaultColor)
        return
    }
    
    public func setPlaceHolderColor( withColorSet name : String, defaultColor : UIColor) -> Void {
        color.placeHolder = self.getColor(colorSet: name, defaultColor: defaultColor)
        return
    }
    
    
    private func getColor( colorSet name : String, defaultColor : UIColor) -> UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: name) ?? defaultColor
        } else {
            return defaultColor
        }
    }
    
    //MARK: - Present with SMCustomCareDefaultColor
    public func present(from sender: UIViewController,
                        delegate:SMCustomerCareDelegate,
                        customColor: SMCustomCareDefaultColor,
                        projectId:String,
                        moreInfo:[String:Any]? = nil,
                        animated: Bool = true,
                        completion:(()->Void)? = nil ){
        self.registerZendesk()
        DispatchQueue.main.async {
            let destinationVC =  SMFeedbackViewController.instantiate(from : .SMCustomerCare)
            destinationVC.iColor = customColor
            destinationVC.delegate = delegate
            destinationVC.moreInfo = moreInfo
            destinationVC.projectId = projectId
            let navigationController = UINavigationController.init(rootViewController: destinationVC)
            sender.present(navigationController, animated: animated, completion: completion)
        }
    }
    
    //MARK: - Present with default color

    public func present(from sender: UIViewController,
                        delegate:SMCustomerCareDelegate,
                        projectId:String,
                        moreInfo:[String:Any]? = nil,
                        animated: Bool = true,
                        completion: (() -> Void)? = nil) {
        self.present(from: sender ,
                     delegate: delegate,
                     customColor: self.color,
                     projectId: projectId,
                     moreInfo: moreInfo,
                     animated: animated,
                     completion: completion)
    }
    
}
