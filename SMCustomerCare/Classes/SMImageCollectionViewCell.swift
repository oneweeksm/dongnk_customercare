//
//  SMImageCollectionViewCell.swift
//  AFNetworking
//
//  Created by OW01 on 5/21/20.
//

import UIKit

public class SMImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewLoading : UIView!
    @IBOutlet weak var vContainerView: UIView!
    @IBOutlet weak var imv: UIImageView!
    
    @IBOutlet weak var indicator : UIActivityIndicatorView!
    @IBOutlet weak var viewRetry : UIView!
    
    var onTapRemoveButton: (() -> Void)?
    var onTapRetryButton: (() -> Void)?
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.addShadow()

    }
    
    func addShadow() {
        vContainerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        vContainerView.layer.shadowOpacity = 1
        vContainerView.layer.shadowOffset = CGSize.init(width: -5, height: 5)
        vContainerView.layer.shadowRadius = 8
    }
    
    
    func config(attack:SMAttackment){
        self.imv.image = attack.image()
        self.viewLoading.isHidden = true
        self.viewRetry.isHidden = true
//        attack.completedUpload = {
//            self.viewLoading.isHidden = attack.state == .uploaded
//            self.viewRetry.isHidden = attack.state != .failed
//            if attack.state == .uploading {
//                self.indicator.startAnimating()
//            }else{
//                self.indicator.stopAnimating()
//            }
//        }
    }
    
    
    
    @IBAction func tapRemoveButton(_ sender: AnyObject) {
        onTapRemoveButton?()
    }
    @IBAction func tapRetryButton(_ sender:Any){
        onTapRetryButton?()
    }
}
