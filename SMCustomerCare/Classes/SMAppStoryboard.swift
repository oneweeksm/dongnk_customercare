//
//  SMAppStoryboard.swift
//  SMCustomerCare
//
//  Created by OW01 on 5/21/20.
//

import Foundation

// MARK: - Khai báo các storyboard trong lib ở đây

public enum SMAppStoryboard: String {
    
    case SMCustomerCare
    
    var instance: UIStoryboard {
        let bundle = getSMCustomCareBundlePath()
        return UIStoryboard.init(name: self.rawValue,
                                 bundle: bundle
        )
    }
    
    func viewController<T: UIViewController> ( viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).SMStoryboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
    
    
    
}

public func getSMCustomCareBundlePath() -> Bundle {
    let bundlePath = Bundle.main.path(forResource: "SMCustomerCare", ofType: "bundle")
    return Bundle.init(path: bundlePath ?? "")!
}
    
public func loadImageFromResources(imageName: String ) -> UIImage? {
        let bundle = getSMCustomCareBundlePath()
    let image = UIImage(named: imageName, in: bundle, compatibleWith: nil)
    return image
}
    


extension UIViewController {
    
    class var SMStoryboardID: String {
        return "\(self)"
    }
    
    static func instantiate( from appStoryboard: SMAppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
}
