//
//  SMNewImageCollectionViewCell.swift
//  SMCustomerCare
//
//  Created by OW01 on 5/21/20.
//

import UIKit

class SMNewImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vContainerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        vContainerView.layer.borderWidth = 2
        vContainerView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.33).cgColor
    }

}
