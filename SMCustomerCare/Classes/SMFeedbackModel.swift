//
//  SMFeedbackModel.swift
//  SMCustomerCare
//
//  Created by OW01 on 5/20/20.
//

import Foundation
import SupportSDK

class SMFeedbackModel: NSObject {
    
    var email: String = ""
    var content: String = ""
    var projectId : String = ""
    var attackments : [SMAttackment] = []
    var moreInfo : [String:Any]? = nil
    
    func isValid()->Bool {
        if !email.isValidEmail() {
            return false
        }
        if content.isEmpty {
            return false
        }
//        if attackments.count < 1 {
//            return false
//        }
        return true
    }
    func addImage(url:URL){
        let attack = SMAttackment()
        attack.imageUrl = url
        self.attackments.append(attack)
    }
    func removeImage(index:Int){
        let attack = attackments[index]
        attack.deleteAttackment {
            print("delete done")
        }
        self.attackments.remove(at: index)
    }
    func getResponseAttackments()->[ZDKUploadResponse] {
        var res : [ZDKUploadResponse] = []
        for attack in attackments {
            if let response =  attack.response {
                res.append(response)
            }
        }
        return res
    }
    func haveUploadedAllAttackments()->Bool{
        let rs = attackments.filter{$0.response == nil}
        return rs.count == 0
    }
    func uploadMultiAttackments(completion:@escaping (_ isSuccess:Bool)->Void){
        if attackments.count == 0 {
            completion(true)
            return
        }else{
            DispatchQueue.global(qos: .userInitiated).async {
                let dispatchGroup = DispatchGroup()
                
                for attack in self.attackments {
                    dispatchGroup.enter()
                    attack.upload {
                        print("leave")
                        dispatchGroup.leave()
                    }
                }
                
                dispatchGroup.notify(queue: .main) {
                    //done
                    
                    print("upload all")
                    completion(self.haveUploadedAllAttackments())
                }
            }
        }
    }
}
class SMAttackment : NSObject {
    var imageUrl : URL? = nil
    var state : SMAttackmentUploadState = .uploading {
        didSet{
            self.completedUpload?()
        }
    }
    var response : ZDKUploadResponse? = nil
    
    var completedUpload : (()->Void)?
    
    func upload(completion:@escaping()->Void){
        if response == nil {
            guard let url = imageUrl else {return}
            guard let imageData = UIImage(contentsOfFile: url.path)?.pngData() else {return}
            let uploader = ZDKUploadProvider()
            self.state = .uploading
            let name = String(format: "%d.png", Int(Date().timeIntervalSince1970))
            uploader.uploadAttachment(imageData , withFilename: name, andContentType: "image/png") { response , error  in
                print("upload xong")
                if let error = error {
                    self.state = .failed
                    print("error upload image = \(error.localizedDescription)")
                }else{
                    print("upload thanh cong")
                    self.state = .uploaded
                    self.response = response
                }
                completion()
            }
        }else{
            completion()
        }
    }
    
    
    func deleteAttackment(completion:@escaping()->Void){
        if let response = self.response {
            let uploader = ZDKUploadProvider()
            uploader.deleteUpload(response.uploadToken) { result , error  in
                print("result = \(result ?? "nil")")
                print("error = \(error?.localizedDescription ?? "nil")")
                completion()
            }
        }else{
            completion()
        }
    }
    
    func image()->UIImage? {
        if let url = imageUrl {
            return UIImage(contentsOfFile: url.path)
        }else{
            return nil 
        }
    }
}
enum SMAttackmentUploadState {
    case  uploading , uploaded , failed
}
