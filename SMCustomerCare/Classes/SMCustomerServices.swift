//
//  SMCustomerServices.swift
//  AFNetworking
//
//  Created by OW01 on 5/21/20.
//

import Foundation
import CoreTelephony
import SupportSDK
import ZendeskCoreSDK

protocol SMCustomerServicesProtocol {
    
}

struct SMDeviceInfo {
    //    let appVersionString: String =
    
    static let bundleID: String = Bundle.main.bundleIdentifier ?? "Unknow bundle ID"
    
    static let appVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    
    static let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    
    static let osVersion = UIDevice.current.systemVersion
    
    static let deviceType:String = UIDevice.current.userInterfaceIdiom == .phone ? "iPhone" : "iPad"
    
    static let deviceName = UIDevice.modelName

    static let usedSpaceInGB = UIDevice.current.freeDiskSpace()
    
    static let totalSpaceInGB = UIDevice.current.totalDiskSpace()
    
    static let systemTime: String = "\(Date().string(fm: "yyyy-MM-dd HH:mm:ss"))"
    
    static var Timezone_of_device: String { return TimeZone.current.abbreviation() ?? "" }
    
    static let screenResolution = "\(UIScreen.main.bounds.width) / \(UIScreen.main.bounds.height)"
    
    static let Language = Locale.preferredLanguages[0] as String
    
    
    static func Mobile_carrier() -> String {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        return carrier?.carrierName ?? "Unknow"
    }
    
    static let Processor_core_count = "\(ProcessInfo().processorCount)"
    
    static func System_theme() -> String{
        return checkDarkTheme() ? "Dark" : "Light"
        
    }
    
}

class SMCustomerServices: NSObject, SMCustomerServicesProtocol {
    class func createIdentify(email:String){
        let identity = Identity.createAnonymous(email: email)
        Zendesk.instance?.setIdentity(identity)
    }
    
    class func postToZendesk(model:SMFeedbackModel,completion:@escaping (_ isSuccess:Bool,_ error:Error?)->Void){
        
        var formatedContent = SMCustomerServices.formatContent(content: model.content)
        if let info = model.moreInfo {
            formatedContent += self.addMoreInfo(info: info)
        }
        
        let request = ZDKCreateRequest()
        request.subject = "[iOS] Ticket number #\(Date().string(fm: "ddMMyy-HHmmss"))"
        request.requestDescription = formatedContent
        request.tags = [model.projectId]
        request.attachments = model.getResponseAttackments()
        
        let provider = ZDKRequestProvider()
        provider.createRequest(request) { result, error in
            if let error = error {
                print("error = \(error.localizedDescription)")
                completion(false,error)
            }else{
                completion(true,nil)
            }
        }
    }
    
    
    static func formatContent(content: String,moreInfo:[String:Any]? = nil) -> String {
        return "\(content)\n" + addLine()
            
        + (addItem(key: "App name", value: Bundle.appName()))
            
        + (addItem(key: "Bundle ID", value: SMDeviceInfo.bundleID))
            
        + (addItem(key: "App version", value: SMDeviceInfo.appVersion))
            
        + (addItem(key: "Build number", value: SMDeviceInfo.buildNumber))

        + (addItem(key: "OS version", value: SMDeviceInfo.osVersion))

        + (addItem(key: "Device Type", value: SMDeviceInfo.deviceType))

        + (addItem(key: "Device Name", value: SMDeviceInfo.deviceName))

        + (addItem(key: "Used space in GB", value: SMDeviceInfo.usedSpaceInGB))

        + (addItem(key: "Total space in GB", value: SMDeviceInfo.totalSpaceInGB))

        + (addItem(key: "System theme", value: SMDeviceInfo.System_theme()))

        + (addItem(key: "System time", value: SMDeviceInfo.systemTime))

        + (addItem(key: "Timezone of device", value: SMDeviceInfo.Timezone_of_device))

        + (addItem(key: "Screen resolution", value: SMDeviceInfo.screenResolution))

        + (addItem(key: "Language", value: SMDeviceInfo.Language))

        + (addItem(key: "Mobile carrier", value: SMDeviceInfo.Mobile_carrier()))

        + (addItem(key: "Processor core count", value: SMDeviceInfo.Processor_core_count))
        
        
        
    }
    
    static func addItem(key: String, value: Any) -> String {
        return "- \(key)" + " : " + "\(value)\n"
    }
    
    
    static func addLine() -> String {
        return "***********Log Info**********\n"
    }
    
    static func addMoreInfo(info:[String:Any])->String{
        let str = (info.compactMap({ (key, value) -> String in
            return (addItem(key: key, value: value))
        }) as Array).reversed().joined()
        return "***********More Info**********\n" + str
    }
    
}

