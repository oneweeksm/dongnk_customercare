//
//  SMCustomerCareConfig.swift
//  SMCustomerCare
//
//  Created by OW01 on 5/20/20.
//

import Foundation
import UIKit

open class SMCustomerCareConfig : NSObject {
    
    public static let shared = SMCustomerCareConfig()
    
}

public struct SMCustomCareDefaultColor {
    
    var navigationBar: UIColor = UIColor(red: 0.969, green: 0.969, blue: 0.969, alpha: 1.0)
    var tint: UIColor = .systemBlue
    var background: UIColor = .white
    var text: UIColor = .black
    var placeHolder = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
    
    init(){
        
    }
    public init( navColor: UIColor, tintColor: UIColor, bgColor: UIColor, txtColor: UIColor, placeHolderColor: UIColor) {
        self.navigationBar = navColor
        self.tint = tintColor
        self.background = bgColor
        self.text = txtColor
        self.placeHolder = placeHolderColor
    }
}

public protocol SMColorProtocol {
    var getAssetColorName: String { get }
    var defaultColor: UIColor { get }
}

struct ZendeskInfo {
    static let url = "https://smartmovevn.zendesk.com"
    static let appId = "405d23c64cd786becd3c0703b8cf6465eb22455b2f934df2"
    static let clientId = "mobile_sdk_client_f1e3bd42d78657f2225c"
}

extension UIColor {
    
    public static func appColor(_ asset: SMColorProtocol) -> UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: asset.getAssetColorName) ?? asset.defaultColor
        } else {
            // Fallback on earlier versions
            return asset.defaultColor
        }
    }
    
}
