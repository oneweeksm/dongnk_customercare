# SMCustomerCare

[![CI Status](https://img.shields.io/travis/oneweekstudio/SMCustomerCare.svg?style=flat)](https://travis-ci.org/oneweekstudio/SMCustomerCare)
[![Version](https://img.shields.io/cocoapods/v/SMCustomerCare.svg?style=flat)](https://cocoapods.org/pods/SMCustomerCare)
[![License](https://img.shields.io/cocoapods/l/SMCustomerCare.svg?style=flat)](https://cocoapods.org/pods/SMCustomerCare)
[![Platform](https://img.shields.io/cocoapods/p/SMCustomerCare.svg?style=flat)](https://cocoapods.org/pods/SMCustomerCare)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Demo ảnh screenshot


One   | Two   | Three 
:-----:| :-----: |:-----:
![picture](screenshots/screen0.png)  |  ![picture](screenshots/screen1.png) | ![picture](screenshots/screen2.png)


## Requirements
```
- iOS 11+
- Swift 5+
- Xcode 12+

```
## Installation
Bước 1:
SMCustomerCare is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SMCustomerCare', :git => 'https://dongnguyendinh@bitbucket.org/oneweeksm/dongnk_customercare.git', :branch => 'master'
```
⇒ Nếu pod bị lỗi minimum version thì vào Podfile chỉnh platform lên iOS 11 và chỉnh Deployment Target của project lên iOS 11 

Bước 2:
Thêm quyền sử dụng photo library vào trong info.plist
```ruby
<key>NSPhotoLibraryUsageDescription</key>
<string>This app wants to use your photos.</string>
```

Bước 3:
Import thằng này vào project
```ruby
import SMCustomerCare
```

Bước 4:
Thực hiện gọi thằng feedback , project ID thì hỏi AM  hoặc Tester  (là Tag trên Zendesk)
```ruby
let vc = SMCustomerCare()
vc.present(from: self , delegate: self, projectId: "<Project ID>")
```

Delegate
```ruby
extension ViewController : SMCustomerCareDelegate {

    //Khi người dùng ấn nút show feedback
    func smCustomerCareDidPresentFeedback() {
        print("-------> smCustomerCareDidPresentFeedback")
    }
    
    //Khi người dùng close màn feedback
    func smCustomerCareDidClose() {
        print("-------> smCustomerCareDidClose")
    }
    
    //Khi người dùng gửi feedback
    func smCustomerCareDidSend() {
        print("-------> smCustomerCareDidSend")
    }
    
    //Khi người dùng thêm ảnh
    func smCustomerCareDidAddImage() {
        print("-------> smCustomerCareDidAddImage")
    }
    
    //Khi người dùng gửi thảnh công lên server
    func smCustomerCareDidSendFeedbackSuccess() {
        print("-------> smCustomerCareDidSendFeedbackSuccess")
    }
    
    //Khi người dùng gửi feed back lỗi trên server
    func smCustomerCareDidSendFeedbackError(_ optionalError : Error?) {
        print("-------> smCustomerCareDidSendFeedbackError : \(optionalError?.localizedDescription ?? "nil")")
    }
    
}
```


Nếu project nào yêu cầu gửi thêm thì Developer có thể gửi kèm thông qua biến "more info", mặc định biến này là nil , ví dụ: 

```ruby
//More Info
func showFeedBackWithMoreInfo() {
    
    let newFeed = SMCustomerCare()
    newFeed.present(from: self , delegate: self , projectId: "<Project ID>", moreInfo: ["More Info Key":"More Info Value","Key 2":"Value 2"])
}
```



Các ví dụ về cách gọi SMFeedback:
```ruby
//Pháp 1
func showFeedBackWithDefault() {
    //Default ( light Mode )
    let vc = SMCustomerCare()
    vc.present(from: self , delegate: self)

}

//Pháp 2
func showFeedBackWithDarkMode() {
    
    let feed2  = SMCustomerCare.init()
    
    let color = SMCustomCareDefaultColor.init(navColor:         UIColor.appColor(CTAssetColor.navigationBackground),
                                              tintColor:        UIColor.appColor(CTAssetColor.title),
                                              bgColor:          UIColor.appColor(CTAssetColor.background),
                                              txtColor:         UIColor.appColor(CTAssetColor.title),
                                              placeHolderColor: UIColor.appColor(CTAssetColor.placeHolder))
    DispatchQueue.main.async {
        feed2.present(from: self , delegate: self , customColor: color, projectId: "<Project ID>" , animated: true , completion: nil)
    }
}

//Pháp 3
func showFeedBackWithDarkMode2() {
    
    let newFeed = SMCustomerCare()
        .setBackground(withColorSet: "background", defaultColor: UIColor.red)
        .setNavigationBackground(withColorSet: "navigationBackground", defaultColor: UIColor.blue)
        .setNavigationTint(withColorSet: "navigationTint", defaultColor: .yellow)
        .setText(withColorSet: "title", defaultColor: .cyan)
        .setPlaceHolder(withColorSet: "placeHolder", defaultColor: .lightGray)
    
    DispatchQueue.main.async {
        newFeed.present(from: self , delegate: self, projectId: "<Project ID>")
    }
}

//Pháp 4
func showFeedBackWithDarkMode4() {
    
    let newFeed = SMCustomerCare()
    
    newFeed.setBackgroundColor(withColorSet: "background", defaultColor: UIColor.red)
    newFeed.setNavigationBackgroundColor(withColorSet: "navigationBackground", defaultColor: UIColor.blue)
    newFeed.setNavigationTintColor(withColorSet: "navigationTint", defaultColor: .yellow)
    newFeed.setTextColor(withColorSet: "title", defaultColor: .cyan)
    newFeed.setPlaceHolderColor(withColorSet: "placeHolder", defaultColor: .lightGray)
    
    DispatchQueue.main.async {
        newFeed.present(from: self , delegate: self, projectId: "<Project ID>")
    }
}

```


## Author : ONEWEEK STUDIO
- Trungnk@smartmove.com.vn

## License

SMCustomerCare is available under the MIT license. See the LICENSE file for more info.
